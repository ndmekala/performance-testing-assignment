class Sale < ActiveRecord::Base
  belongs_to :customer


  paginates_per 25

  def amount_with_tax
    self.amount + self.tax
  end
end
